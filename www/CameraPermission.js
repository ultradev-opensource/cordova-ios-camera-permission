var exec = require('cordova/exec');

exports.request = function (cb) {
    function onResultOk(data) {
      cb("");
    }
    function onResultError(err) {
      cb(err);
    }
    exec(onResultOk, onResultError, 'CameraPermission', 'request', [camera]);
};
