import Foundation
import AVFoundation

struct Log: TextOutputStream {
    func write(_ string: String) {
        let fm = FileManager.default
        //let log = fm.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("log.txt")
        let paths = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)
        let documentDirectoryPath = paths.first!
        let log = documentDirectoryPath.appendingPathComponent("log.txt")
        if let handle = try? FileHandle(forWritingTo: log) {
            handle.seekToEndOfFile()
            handle.write(string.data(using: .utf8)!)
            handle.closeFile()
        } else {
            try? string.data(using: .utf8)?.write(to: log)
        }
    }
}

var logger = Log()

@objc(CameraFrames)
class CameraFrames: CDVPlugin {

    private var permissionGranted = false
    private var mainCommand: CDVInvokedUrlCommand?
    private var isSendingFrames = false
    func customlog(str: String) {
        print(str, Date(), to: &logger)
    }

    func errback(str: String) {
        customlog(str: str)
        var pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR, messageAs: str
        )
        self.commandDelegate!.send(pluginResult, callbackId: mainCommand?.callbackId)
    }

    func succallback(str: String) {
      var pluginResult = CDVPluginResult(
          status: CDVCommandStatus_OK, messageAs: str
      )
      self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
    }

    @objc(request:)
    func request(command: CDVInvokedUrlCommand) {
        customlog(str: "Request permission")
        mainCommand = command
        checkPermission(command: command)
    }

    private func checkPermission(command: CDVInvokedUrlCommand) {
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            // The user has previously granted access to the camera.
            customlog(str: "Camera previously authorized")
            permissionGranted = true
            self.succallback(str: "Sucesso")
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. We suspend the session queue to delay session
             setup until the access request has completed.

             Note that audio access will be implicitly requested when we
             create an AVCaptureDeviceInput for audio during session setup.
             */
            customlog(str: "Requesting camera access")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] granted in
                if !granted {
                    self.errback(str: "Access not granted")
                    self.permissionGranted = false
                } else {
                    self.customlog(str: "Access granted")
                    self.permissionGranted = true
                    self.succallback(str: "Sucesso")
                }
            })
        default:
            // The user has previously denied access.
            errback(str: "User denied camera access")
            //commandDelegate.evalJs("alert('User denied camera access')")
            permissionGranted = false
        }
    }
}
