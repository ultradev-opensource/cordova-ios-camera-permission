# Camera Camera Permission Plugin for Apache Cordova

The plugin provides a simple JavaScript API for request iOS camera permission.


## Supported Platforms

* iOS


# Installing

### Cordova

    $ cordova plugin add https://gitlab.com/ultradev-opensource/cordova-ios-camera-permission.git

## Usage

The plugin exposes the `cordova.plugins.CameraPermission` JavaScript namespace which contains the following function.

# Request camera permission

```javascript

cordova.plugins.CameraPermission.request(function (err) {
  if(!err) {
    console.log("Permission granted");
  } else {
    console.log(err);
  }
});

```


## Author

[Milseg](https://github.com/milseg)

### Maintainers

* [Milseg](https://github.com/milseg)


## License

[ISC](./LICENSE.md)
